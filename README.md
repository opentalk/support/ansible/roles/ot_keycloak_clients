# ot_keycloak_clients

An Ansible role to create and configure OpenTalk clients within an existing Keycloak realm.

## Examples

An example playbook can be found [here](https://gitlab.opencode.de/opentalk/support/ansible/playbooks/keycloak_setup)

## Currently availabe variables

| variable                             | type   | default value                                             |descriptiom                                    | mandatory |
|--------------------------------------|--------|-----------------------------------------------------------|-----------------------------------------------|-----------|
| OPENTALK_BASE_DOMAIN                 | string | opentalk.example.com                                      | domain used for the OpenTalk WebUI            | x         |
| AUTH_KEYCLOAK_URL                    | string | https://keycloak.opentalk.example.com/auth                | keycloak URL to authenticate                  | x         |
| AUTH_REALM                           | string | master                                                    | realm to authenticate                         | x         |
| AUTH_USERNAME                        | string | admin                                                     | admin user                                    | x         |
| AUTH_PASSWORD                        | string | SECRET                                                    | admin password                                | x         |
| KEYCLOAK_TARGET_REALM                | string | {{ AUTH_REALM }}                                          | the realm to create the resources in          |           |
|                                      |        |                                                           | AUTH_REALM is used if not defined             |           |
| BACKEND_CLIENT_ID                    | string | OtBackend                                                 | name of the backend client                    |           |
| BACKEND_CLIENT_SECRET                | string | SECRET                                                    | secret of the backend client                  | x         |
| BACKEND_CLIENT_REDIRECT_URIS         | list   | ["https://{{ OPENTALK_BASE_DOMAIN }}"]                    | a list of valid redirect URIs                 |           |
| BACKEND_CLIENT_WEB_ORIGINS           | list   | []                                                        | a list of alowed CORS origins                 |           |
| FRONTEND_CLIENT_ID                   | string | OtFrontend                                                | name of the frontend client                   |           |
| FRONTEND_CLIENT_REDIRECT_URIS        | list   | [                                                         |                                               |           |
|                                      |        |  "https://{{ OPENTALK_BASE_DOMAIN }}/auth/popup_callback",| a list of valid redirect URIs                 |           |
|                                      |        |  "https://{{ OPENTALK_BASE_DOMAIN }}/auth/callback",      |                                               |           |
|                                      |        |  "https://{{ OPENTALK_BASE_DOMAIN }}/dashboard",          |                                               |           |
|                                      |        |  "https://{{ OPENTALK_BASE_DOMAIN }}/"                    |                                               |           |
|                                      |        | ]                                                         |                                               |           |
| FRONTEND_CLIENT_WEB_ORIGINS          | list   |["https://{{ OPENTALK_BASE_DOMAIN }}"]                     | a list of alowed CORS origins                 |           |
| OBELISK_CLIENT_ID                    | string | Obelisk                                                   | name of the obelisk client                    |           |
| OBELISK_CLIENT_SECRET                | string | SECRET                                                    | secret of the obelisk client                  | x         |
| OBELISK_CLIENT_REDIRECT_URIS         | list   | []                                                        | a list of valid redirect URIs                 |           |
| OBELISK_CLIENT_WEB_ORIGINS           | list   | []                                                        | a list of alowed CORS origins                 |           |
| RECORDER_CLIENT_ID                   | string | Recorder                                                  | name of the recorder client                   |           |
| RECORDER_CLIENT_SECRET               | string | SECRET                                                    | secret of the recorder client                 | x         |
| RECORDER_CLIENT_REDIRECT_URIS        | list   | []                                                        | a list of valid redirect URIs                 |           |
| RECORDER_CLIENT_WEB_ORIGINS          | list   | []                                                        | a list of alowed CORS origins                 |           |
| REALM_MANAGEMENT_CLIENT_ID           | string | realm-management                                          | name of the realm-management client           |           |
| REALM_MANAGEMENT_REDIRECT_URIS       | list   | []                                                        | a list of valid redirect URIs                 |           |
| REALM_MANAGEMENT_WEB_ORIGINS         | list   | []                                                        | a list of alowed CORS origins                 |           |
| REALM_MANAGEMENT_ROLES_NO_COMPOSITES | list   | []                                                        | a list of roles to create without composities |           |
| REALM_MANAGEMENT_REALM_ADMIN_ROLES   | list   | []                                                        | a list of roles to assign to realm-admin role |           |
| TESTUSER_CREATE                      | bool   | false                                                     | create and manage the testuser                |           |
| TESTUSER_ENABLE                      | bool   | true                                                      | enable the testuser                           |           |
| TESTUSER_PASSWORD                    | string | SECRET                                                    | set testuser password                         |           |
